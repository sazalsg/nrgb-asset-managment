// Copyright (c) 2016, Frappe Technologies Pvt. Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["Asset List"] = {
	"filters": [

		{
			label:__('Company'),
			fieldtype:"Link",
			fieldname:"company",
			options:"Company",
			default:"NRBG Global Bank Ltd .."

		},
		{
			label:__('Purchase Form Data'),
			fieldtype:"Date",
			fieldname:"purchase_form_date",
		},
		{
			label:__('Purchase To Data'),
			fieldtype:"Date",
			fieldname:"purchase_to_date",
		}
	]
}
