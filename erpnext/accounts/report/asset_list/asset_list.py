# Copyright (c) 2013, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _

def execute(filters=None):
	
	return columns(), data(filters)

def columns():
	colum=[_("Asset ID"+":Link/Asset:150"),_('Asset Name'+':Data:150'),_('Item Code'+':Link/Item:150'),_('Item Name'+':Data:150')
		,_('Depreciations Method'+':Data:150')
		,_('staus'+':Data:150'),_('Gross Purchase Amount'+':Data:150'),_('Company'+':Data:150')
		,_('Warehouse'+':Data:150'),_('Purchase Date'+':Data:150')]
	return colum

def data(filters):
	if(filters.get('purchase_form_date')):
		filters["purchase_date"] = ('>=', filters.purchase_form_date)
		del filters['purchase_form_date']
		
	if (filters.get('purchase_to_date')):
		filters["purchase_date"] = ('<=', filters.purchase_to_date)
		del filters['purchase_to_date']
		
	data=frappe.get_all('Asset',filters=filters,fields=['name','asset_name','item_code','item_name','depreciation_method','status',
	                                               'gross_purchase_amount','company','warehouse','purchase_date'],as_list=True)
	return data
	

