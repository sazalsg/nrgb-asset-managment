# Copyright (c) 2013, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _

def execute(filters=None):
	
	return columns(), data(filters)

def columns():
	colum=[_('Assign Code'+":Link/Software License Assign:100"),_('Item Code'+':Data:150'),_('License Code'+':Data:150')
	       ,_('Software Name'+':Data:150'),_('Licensed Key'+':Data:150'),_('Software Type'+':Data:150'),_('Software Category'+':Data:150'),_('Licensed To Name'+':Data:150')
	       ,_('Licensed To Email'+':Data:150')]
	
	return colum

def data(filters):
	data=frappe.get_all('Software License Assign',fields=['name','software_item','license','name1','licensed_key','software_type','software_category','licensed_to_name','licensed_to_email'],filters=filters,as_list=True)
	
	return data
