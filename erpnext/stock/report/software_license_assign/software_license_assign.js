// Copyright (c) 2016, Frappe Technologies Pvt. Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["Software License Assign"] = {
	"filters": [
		{
			label:__('License Key'),
			fieldtype:"Data",
			fieldname:"licensed_key",

		},

	]
}
