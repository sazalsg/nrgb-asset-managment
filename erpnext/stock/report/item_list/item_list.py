# Copyright (c) 2013, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _

def execute(filters=None):
	
	return columns(), data(filters)

def columns():
	
	colum=[_('Item Code'+':Link/Item:150'),_('Item Name'+':Data:150'),_('Item Group'+':Data:150')
		,_('Default Unit of Measure'+':Data:150')
		,_('Default Warehouse'+':Data:150')
	    ,_('Default Supplier'+':Data:150')
	    ,_('Description'+':Data:150')
	    ,_('Brand'+':Data:150')]
	return colum

def data(filters):
	data=frappe.get_all('Item',fields=['name','item_name','item_group','stock_uom','default_warehouse','default_supplier','description','brand'],filters=filters,as_list=True)
	return data