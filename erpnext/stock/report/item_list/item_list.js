// Copyright (c) 2016, Frappe Technologies Pvt. Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["Item List"] = {
	"filters": [
		{
			label:__('Item Group'),
			fieldtype:"Link",
			fieldname:"item_group",
			options:"Item Group"
		},
		{
			label:__('Default Warehouse'),
			fieldtype:"Link",
			fieldname:"default_warehouse",
			options:"Warehouse"
		},
	]
}
