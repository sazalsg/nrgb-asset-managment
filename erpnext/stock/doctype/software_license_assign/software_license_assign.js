// Copyright (c) 2016, Frappe Technologies Pvt. Ltd. and contributors
// For license information, please see license.txt

frappe.ui.form.on('Software License Assign', {
	onload: function(frm) {

	    frm.set_query('software_item',function()
		{
			return{
				filters:[
					{"is_software":1}
				]
			}
		});

	    frm.set_query('license',function()
		{
			return{
				filters:[
					{"is_software_license":1}
				]
			}
		});
	},
	software_item:function(frm)
	{

		 frappe.call({
            "method": "frappe.client.get",
            args: {
                doctype: "Item",
                name: frm.doc.software_item
            },
            callback: function (data)
            {
				console.log(data)
				frm.set_value('name1', data.message.name),
				frm.set_value('software_type', data.message.software_type),
				frm.set_value('software_category', data.message.software_category),
				frm.set_value('item_group', data.message.item_group),
				frm.set_value('software_expire_date', data.message.expiry_date)
            }
        })
	},
	license:function(frm)
	{

		 frappe.call({
            "method": "frappe.client.get",
            args: {
                doctype: "Item",
                name: frm.doc.license
            },
            callback: function (data)
            {
				console.log(data)
				frm.set_value('licensed_to_name', data.message.licensed_to_name),
				frm.set_value('licensed_to_email', data.message.licensed_to_email),
				frm.set_value('licensed_type', data.message.licensed_type),
				frm.set_value('licensed_key', data.message.licensed_key),
				frm.set_value('seats', data.message.seats),
					frm.set_value('expiry_date',data.message.licensed_expiry_date)
            }
        })
	}
});
